"""Accessing or extracting the same things from multiple passed objects.

These are not coroutines since there is no meaningful internal state,
just yields out once for each item.

If the underlying source of data is an iterator, just don't force it/
request everything from these generators all at once, use next()
instead.
"""
def item_lists(objs, keys):
    """
    Keys are something like list indices or dict keys, used to access
    different things from the underlying object.
    """
    for obj in objs:
        extracted = [obj[key] for key in keys]
        yield extracted


def slices(objs, start=None, stop=None, step=None):
    """
    """
    for obj in objs:
        extracted = obj[start:stop:step]
        yield extracted


def attr_lists(objs, attrs):
    """Yield specified attr values from each sent object.
    """
    assert all(isinstance(attr, type('')) for attr in attrs)
    for obj in objs:
        extracted = [getattr(obj, attr) for attr in attrs]
        yield extracted


def item_dicts(objs, keys):
    """
    Keys are something like list indices or dict keys, used to access
    different things from the underlying object.
    """
    for obj in objs:
        extracted = {key: obj[key] for key in keys}
        yield extracted


def attr_dicts(objs, attrs):
    """Yield specified attr values from each sent object.
    """
    assert all(isinstance(attr, type('')) for attr in attrs)
    for obj in objs:
        extracted = {attr: getattr(obj, attr) for attr in attrs}
        yield extracted
