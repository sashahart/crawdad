from crawdad import access


class TestItem(object):

    list_data = (
        [0, 1, 2, 3],
        [0, 4, 5, 6],
    )

    list_keys = (1, 3)

    dict_data = (
        {'a': 2, 'b': 3, 'c': 4},
        {'a': 4, 'b': 5, 'c': 6},
    )

    dict_keys = ['a', 'b']

    def test_lists_from_lists(self):
        it = access.item_lists(self.list_data, self.list_keys)
        assert next(it) == [1, 3]
        assert next(it) == [4, 6]

    def test_lists_from_dicts(self):
        it = access.item_lists(self.dict_data, self.dict_keys)
        assert next(it) == [2, 3]
        assert next(it) == [4, 5]

    def test_dicts_from_lists(self):
        it = access.item_dicts(self.list_data, self.list_keys)
        assert next(it) == {1: 1, 3: 3}
        assert next(it) == {1: 4, 3: 6}

    def test_dicts_from_dicts(self):
        it = access.item_dicts(self.dict_data, self.dict_keys)
        assert next(it) == {'a': 2, 'b': 3}
        assert next(it) == {'a': 4, 'b': 5}


def test_slice_lists():
    data = (
        [0, 1, 2, 3],
        [0, 4, 5, 6],
    )
    it = access.slices(data, 1, 3)
    assert next(it) == [1, 2]
    assert next(it) == [4, 5]


class Dummy(object):
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class TestAttr(object):
    data = (
        Dummy(name="sam", age=90, height=15),
        Dummy(name="jim", age=87, height=2),
        Dummy(name="max", age=16, height=600),
    )

    keys = ('age', 'name')

    def test_lists(self):
        it = access.attr_lists(self.data, self.keys)
        assert next(it) == [90, "sam"]
        assert next(it) == [87, "jim"]
        assert next(it) == [16, "max"]


    def test_dicts(self):
        data = [
            Dummy(name="sam", age=90, height=15),
            Dummy(name="jim", age=87, height=2),
            Dummy(name="max", age=16, height=600),
        ]
        it = access.attr_dicts(self.data, self.keys)
        assert next(it) == {"age": 90, "name": "sam"}
        assert next(it) == {"age": 87, "name": "jim"}
        assert next(it) == {"age": 16, "name": "max"}
