from crawdad import variability


class TestVariance(object):

    def test_sample(self):
        g = variability.variance(sample=True)
        # variance for sample not defined with one value.
        assert g.send(1) == None
        assert g.send(2) == 0.5
        assert g.send(3) == 1

    # case from http://www.python.org/dev/peps/pep-0450/
    def test_pep450(self):
        data = [1, 2, 4, 5, 8]
        g = variability.variance(sample=True)
        for datum in data:
            result = g.send(datum)
        assert result == 7.5

    # case from http://www.python.org/dev/peps/pep-0450/
    def test_pep450_added_constant(self):
        data = [1, 2, 4, 5, 8]
        g = variability.variance(sample=True)
        for datum in data:
            result = g.send(datum + 1e12)
        # PEP 450's concern is getting 0
        assert result != 0.0
        # I am pretty happy from implementation giving 7.4999,
        # OTOH don't want significant backsliding from this
        assert abs(result - 7.5) < 0.0001

    # case from http://www.python.org/dev/peps/pep-0450/
    def test_pep450_not_negative(self):
        data = [1, 2, 4, 5, 8]
        g = variability.variance(sample=True)
        for i in range(0, 100):
            for datum in data:
                result = g.send(datum)
        assert result > 0
        # similar to result from PEP 450
        assert abs(result - 6.012) < 0.001



def test_stddev():
    g = variability.stddev()
    assert g.send(2) == 0
    assert g.send(4) == 1
    for n in (4, 4, 5):
        g.send(n)
    assert g.send(5) == 1
    g.send(7)
    assert g.send(9) == 2
