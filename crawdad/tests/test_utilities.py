from crawdad.prime import prime
from crawdad import utilities


@prime
def identity():
    value = None
    while True:
        value = yield value


@prime
def record():
    values = []
    while True:
        value = yield values
        values.append(value)


@prime
def adder(constant):
    value = 0
    while True:
        value = (yield value + constant) or 0


def test_splay():
    g = utilities.splay(identity(), identity())
    assert g.send(1) == (1, 1)


def test_pipeline():
    a = adder(1)
    b = adder(2)
    c = adder(3)
    p = utilities.pipeline(a, b, c)
    assert p.send(100) == 106


class TestLag(object):
    def test_0(self):
        g = utilities.lag(0)
        assert g.send(1) == 1
        assert g.send(2) == 2
        assert g.send(3) == 3

    def test_1(self):
        g = utilities.lag(1)
        assert g.send(1) == None
        assert g.send(2) == 1
        assert g.send(3) == 2

    def test_2(self):
        g = utilities.lag(2)
        assert g.send(1) == None
        assert g.send(2) == None
        assert g.send(3) == 1
        assert g.send(4) == 2


def test_guard():
    # particularly, the first case should NOT give us None

    def predicate(value):
        return (value > 10)

    recorder = record()

    g = utilities.guard(recorder, predicate)
    assert g.send(20) == [20]
    assert g.send(9) == None
    assert g.send(30) == [20, 30]
    assert g.send(-1) == None


def test_sort():
    g = utilities.sort()
    assert g.send(2) == [2]
    assert g.send(3) == [2, 3]
    assert g.send(1) == [1, 2, 3]
