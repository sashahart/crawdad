from decimal import Decimal
from crawdad import central_tendency
from crawdad import utilities


class TestMean(object):

    def test_trivial(self):
        g = central_tendency.mean()
        assert g.send(1) == 1
        assert g.send(-1) == 0

    def test_magnitude(self):
        g = central_tendency.mean()
        data = [1e30, 1, 3, -1e30]
        for datum in data:
            result = g.send(datum)
        assert result == 1


def test_majority():
    g = central_tendency.majority()
    assert g.send(1) == 1
    assert g.send(1) == 1
    assert g.send(2) == 1
    assert g.send(2) == 1
    assert g.send(1) == 1
    assert g.send(2) == 1
    assert g.send(2) == 2


def test_ema():
    g = central_tendency.ema(alpha=0.5)
    assert g.send(1) == 1
    assert g.send(2) == 1.5


def test_geometric_mean_trivial():
    g = central_tendency.geometric_mean()
    assert g.send(2) == 2
    assert g.send(8) == 4


def test_geometric_mean_float():
    g = central_tendency.geometric_mean()
    g.send(4)
    g.send(1)
    assert g.send(1/32.0) == 0.5


def test_geometric_mean_decimal():
    g = central_tendency.geometric_mean()
    g.send(4)
    g.send(1)
    assert g.send(Decimal("0.03125")) == 0.5
