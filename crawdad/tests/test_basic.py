from crawdad import basic


def test_count(upto=10):
    g = basic.count()
    for i in range(1, upto + 1):
        assert g.send(i) == i


class TestSum(object):

    def test_trivial(self):
        """Really simple test any sum algorithm should pass easily.

        That includes the naive sum algorithm.
        """
        numbers = (2, 3, 5, 7, 11)
        g = basic.sum()
        real_sum = 0
        for number in numbers:
            assert g.send(0) == real_sum
            real_sum += number
            assert g.send(number) == real_sum

    def test_ten_tenths(self):
        """
        The naive sum algorithm is susceptible to this issue, but simple
        Kahan summation is not.
        """
        g = basic.sum()
        values = [0.1] * 10
        for value in values:
            result = g.send(value)
        assert result == 1.0

    def test_catastrophic_cancellation(self):
        """
        simple Kahan summation is susceptible to this issue, and
        similarly fouls up mean calculations.
        """
        g = basic.sum()
        n = 1000
        for i in range(0, n):
            g.send(1)
            g.send(1e100)
            g.send(1)
            result = g.send(-1e100)
        assert abs(result - (n*2)) < 1


def test_max():
    g = basic.max()
    assert g.send(-1) == -1
    assert g.send(0) == 0
    assert g.send(1) == 1
    assert g.send(0) == 1
    assert g.send(2) == 2


def test_min():
    g = basic.min()
    assert g.send(1) == 1
    assert g.send(0) == 0
    assert g.send(-1) == -1
    assert g.send(0) == -1
    assert g.send(-2) == -2


def test_min_and_max():
    g = basic.min_and_max()
    assert g.send(0) == (0, 0)
    assert g.send(1) == (0, 1)
    assert g.send(2) == (0, 2)
    assert g.send(-1) == (-1, 2)
    assert g.send(-2) == (-2, 2)
