"""Coroutines calculating measures of variability/disperson
"""
import math
from crawdad import prime


@prime
def variance(sample=False):
    """Variance coroutine.

    Based on http://www.johndcook.com/standard_deviation.html.
    """
    count = 1
    state = 0.0
    # As soon as we receive the first value, set that value as the mean
    # to differ from, then yield 0.0/None unconditionally in response
    # (depending on sample).
    # Then when we receive a second value, we roll into the loop.
    mean = yield None
    initial = 0.0 if not sample else None
    value = yield initial
    # (If we didn't yield that second time above, we'd need the yield in
    # the loop at the top of the loop, and it would need result to have
    # a value before the loop.)

    while True:
        count += 1
        diff = float(value - mean)
        mean = mean + (diff / count)
        state = state + (diff * (value - mean))
        result = (state / (count - 1)) if sample else (state / count)
        value = yield result


@prime
def stddev(sample=False):
    """Standard deviation coroutine
    """
    variance_it = variance(sample=sample)
    value = (yield None)
    while True:
        new_variance = variance_it.send(value)
        value = yield math.sqrt(new_variance)
