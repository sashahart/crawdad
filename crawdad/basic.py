# coding: utf-8
from decimal import Decimal
from fractions import Fraction
from crawdad import prime


@prime
def _min():
    """Yield minimum of items sent so far.

    Before using, first call .next() or .send(None) to initialize
    """
    smallest = (yield None)
    while True:
        next = (yield smallest)
        if next < smallest:
            smallest = next


@prime
def _max():
    """Yield maximum of items sent so far.

    Before using, first call .next() or .send(None) to initialize
    """
    biggest = (yield None)
    while True:
        next = (yield biggest)
        if next > biggest:
            biggest = next


@prime
def min_and_max():
    """Yield (min, max) of items sent so far.
    """
    g_min = _min()
    g_max = _max()
    value = yield None
    while True:
        min_ = g_min.send(value)
        max_ = g_max.send(value)
        value = yield min_, max_


@prime
def _sum_naive():
    """Yield sum of items sent so far.

    Also known as the running total.

    This is just a simple Python implementation. It's not only subject
    to the normal floating point issues, but can be really crippled by
    them in relatively normal cases due to accumulation of error.

    Simply sorting input values isn't a viable fix since this handles
    a stream. math.fsum does not expose a streaming interface.
    """
    total = 0
    while True:
        value = yield total
        total += value


@prime
def _sum_kahan():
    """Yield sum of items sent so far.

    Also known as the running total.

    Uses the Kahan correction algorithm to reduce rounding error. cf.
    http://researcher.watson.ibm.com/researcher/files/us-ytian/
    numerical_stability_icde2012.pdf
    """
    old_sum = 0
    compensation = 0
    while True:
        value = (yield old_sum) + compensation
        new_sum = old_sum + value
        compensation = value - (new_sum - old_sum)
        old_sum = new_sum


@prime
def _sum_kahan_babuska():
    """Yield sum of items sent so far.

    Also known as the running total.

    Uses the 'improved Kahan-Babuška Algorithm', as described in 'A
    generalized Kahan-Babuška-Summation-Algorithm' by Andreas Klein,
    available from http://cage.ugent.be/~klein/papers/floating-point.pdf.
    """
    old_sum = yield None
    compensation = 0
    while True:
        value = (yield old_sum + compensation)
        new_sum = old_sum + value
        if abs(old_sum) > abs(value):
            compensation = compensation + ((old_sum - new_sum) + value)
        else:
            compensation = compensation + ((value - new_sum) + old_sum)
        old_sum = new_sum


@prime
def count():
    """Yield how many items have been sent in so far.

    Before using, first call .next() or .send(None) to initialize
    """
    # yield point for prime to advance to, first sent value advances
    # into loop
    yield
    count = 0
    while True:
        count += 1
        yield count


# probably only expose these from __init__.py
real_sum = sum
min = _min
max = _max
sum = _sum_kahan_babuska
__all__ = []
