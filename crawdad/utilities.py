from crawdad.prime import prime


@prime
def splay(*consumers):
    """Yield results of sending each received item to several consumers.

    For example: splay(a, b, c) returns a generator. Sending
    any item to that generator results in the same item being sent also
    to a, b, and c, and values being collected in response from each.
    The returned value is (value from a, value from b, value from c).

    Used to let multiple downstream coroutines compute on the same
    stream without stealing each others' items or buffering more than
    one item.

    Each is automatically primed with .send(None) before starting.

    Before using, first call .next() or .send(None) to initialize
    """
    yielded = None
    while True:
        item = (yield yielded)
        yielded = tuple(consumer.send(item) for consumer in consumers)


@prime
def pipeline(*consumers):
    """Sent items are fed through sequence of consumers.

    Example: pipeline(a, b, c) sends each item to a, collects the
    result, sends that result to b, collects the result, sends that to
    c, and yields the result from c.
    """
    item = None
    while True:
        item = (yield item)
        for consumer in consumers:
            item = consumer.send(item)


@prime
def guard(consumer, predicate):
    """Sent items are forwarded to consumer if predicate(item) is True.

    Otherwise, the item is not forwarded to the consumer and the yielded
    value is None.
    """
    result = None
    while True:
        value = yield result
        result = consumer.send(value) if predicate(value) else None



@prime
def lag(interval, initial=None):
    """Feed items back out at a delay of (interval) steps.

    For example, a lag(1) will yield back whatever you gave it at the
    last send().

    :arg initial:
        A sequence providing ``interval`` values to yield prior to
        sufficient inputs being sent in.

        Providing a sequence of fewer than interval items as the initial
        ones to yield is confused and will result in a raised error.

        If not provided, the generator will yield None values until it
        has sufficiently many recorded inputs to start yielding back.
    """
    if initial is not None:
        recorded = []
        initial_it = iter(initial)
        for i in range(0, interval + 1):
            recorded.append(next(initial_it))
    else:
        recorded = [None] * (interval + 1)
    while True:
        for index in range(0, interval + 1):
            print(index, recorded)
            value = (yield recorded[index])
            recorded[index] = value


@prime
def sort(initial=None):
    """Maintain sorted list of items sent in.

    Of course this uses lots of memory, since it holds on to all the
    items that were sent in. It also uses a lot of time, since it
    re-sorts the list every time you send something in. Normally you'd
    want to wait and then sort when you need the sorted list.
    """
    # heapq isn't much help because you don't get the items ordered
    # without popping them all and in the end it does not seem to be
    # a time savings. Python's sort algorithm might make this OK.
    current = [item for item in initial] if initial else []
    while True:
        current.sort()
        value = yield current
        current.append(value)
