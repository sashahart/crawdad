from decorator import decorator


@decorator
def prime(generator_function, *args, **kwargs):
    """Decorate a generator to call .send(None) before iterating.
    """
    generator = generator_function(*args, **kwargs)
    generator.send(None)
    return generator
