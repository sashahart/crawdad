"""Coroutines for computing streaming measures of central tendency.
"""
import math
from decimal import Decimal
from crawdad import utilities, basic
from crawdad.prime import prime


@prime
def mean(decimal=False):
    """Yield average (arithmetic mean) of all items sent in so far.

    Also known as the cumulative moving average.
    """
    splayed = utilities.splay(basic.count(), basic.sum())
    result = None
    while True:
        item = (yield result)
        count, total = splayed.send(item)
        if decimal:
            result = total / Decimal(count)
        else:
            result = total / float(count)


@prime
def ema(alpha):
    """Exponential moving average / exponentially-weighted moving average.

    :arg alpha:
        discounting factor. In the limit as alpha approaches 1, old
        values have no influence (it throws away all state). As alpha
        approaches 0, new values have no influence (it learns nothing).
    """
    assert 0 < alpha < 1
    state = (yield None)
    while True:
        value = (yield state)
        state = alpha * value + (1 - alpha) * state


@prime
def majority(start=None):
    """Find an item with more than 50% share of those encountered.

    Unpredictable results if there isn't one. This can approximate a
    mode in some circumstances.

    Based on Cormode and Hadjieleftheriou (2009):
    "Finding the Frequent Items in Streams of Data"
    """
    result = start
    counter = 0
    while True:
        value = (yield result)
        if value == result:
            counter += 1
        elif counter == 0:
            result = value
            counter = 1
        else:
            counter -= 1


@prime
def geometric_mean(decimal=True):
    """Running geometric means.
    """
    splayed = utilities.splay(basic.count(), basic.sum())
    value = yield None
    while True:
        if decimal:
            count, total = splayed.send(Decimal(value).ln())
        else:
            count, total = splayed.send(math.log(value))
        value = (yield math.exp(total / count))
