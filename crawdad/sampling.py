import random
import collections
from crawdad.prime import prime


@prime
def sliding_window(size, trim=False):
    """Coroutine providing windows over the items sent to it.

    For example, [1, 2, 3, 4] might have size=2 sliding windows
    [None, 1], [1, 2], [2, 3], [3, 4].

    :arg trim:
        if False (default), pads out the right of the window with None
        values (e.g., [1, None, None], [1, 2, None], [1, 2, 3]).
        if True, initial windows will be short (e.g. [1], [1, 2], [1, 2,
        3]).
    """
    # deque is about 10 times faster than list to rotate + set last
    window = collections.deque([None] * size)
    # Fill up the buffer
    for i in range(0, size):
        if trim:
            value = (yield [window[j] for j in range(0, i)])
        else:
            value = (yield [item for item in window])
        window[i] = value
    # Then go on forever
    while True:
        value = (yield [item for item in window])
        window.rotate(-1)
        window[-1] = value
    # TODO: specifiable advance interval


# http://stackoverflow.com/questions/10657503/find-running-median-from-a-stream-of-integers
@prime
def reservoir(size, randint=random.randint):
    """Keep a running random sample of encountered items (reservoir sampling).

    This can be used to generate things like median estimates.

    At the beginning (up to ``size`` values sent in), you do not get a random
    sample but the whole list of values sent in.

    Stopping at any given time should give any past value equal
    probability of being in the final reservoir, with later values having no
    greater probability than earlier ones.

    However, *changes* in the reservoir are nonrandom and path
    dependent, a random walk.

    :arg size:
        How many items to keep in the running random sample.
    """
    # Count of items seen so far
    count = 0
    # Running random sample of inputs - might be of any type
    reservoir = [None] * size
    # fill in
    while count < size:
        reservoir[count] = (yield reservoir[:count])
        count += 1
    # keep yielding, replacing elements less and less frequently
    while True:
        value = (yield reservoir[:count])
        p = randint(0, count - 1)
        if p < size:
            reservoir[p] = value
        count += 1


@prime
def draw(size, randint=random.randint, choice=random.choice):
    """Send a value, get a 'random' past value.

    When each new value is sent in, it is added to a reservoir
    (overwriting some previous value chosen at random) and the reservoir
    is used to draw a sample which is yielded back (instead of yielding
    back the whole list, as in the case of reservoir).

    The drawn numbers are really not random with respect to each other.
    The state of the reservoir is path-dependent. The reservoir is not
    of infinite size. Its items are randomly replaced, after which they
    are of course not drawn again. So past random events have an ongoing
    influence on the future, making a subtle relationship among the
    probabilities of draws. Smaller reservoir sizes mean frequent
    replacements and more homogeneity in drawn numbers. Larger reservoir
    sizes mean less frequent replacements, longer lifespans for each
    value but on the other hand, longer-term path dependencies.

    Contrast with a sliding window being used to draw.
    """
    res = reservoir(size, randint=randint)
    values = []
    while True:
        if values:
            drawn = choice(values)
        else:
            drawn = None
        value = yield drawn
        values = res.send(value)


@prime
def chunk(size):
    """Every (size) items sent in, yield last (size) items. else, None.

    This is like sliding_window, but does not give overlapping windows.
    When the latest data does not yet make a new chunk, this yields None.

    This is really only for cases which need the streaming.
    If it is possible to iterate over pre-buffered items (e.g. a list)
    rather than sending in items one at a time, that will be better.
    """
    chunk = None
    while True:
        value = yield chunk
        chunk = [value]
        for i in range(0, size - 1):
            value = (yield None)
            chunk.append(value)
            print(i, chunk)


@prime
def chunk_contiguous(function, limit=None):
    """Yields chunks made up of objects that share some property.

    For each item sent in, function(obj) is called and if the returned value
    is the same as last time, it is added to a buffer (up to limit objects).
    On changes, the buffered chunk is yielded.

    This doesn't address non-contiguous identical values.
    """
    value = yield None
    group = [value]
    last_key = function(value)
    value = yield None
    while True:
        key = function(value)
        if key != last_key or limit is not None and len(group) > limit:
            to_yield = group
            group = []
        else:
            to_yield = None
        group.append(value)
        value = yield to_yield
        last_key = key
