def read_file(filename, bufsize=65536):
    """Generator which opens a file and yields its lines as bytestrings.

    :arg bufsize:
        How much data to ask Python to buffer in the file,
        for faster reads.
    """
    # rb in Python 3 is faster than alternatives, and still gives us
    # proper lines - definitely avoid codecs module unless necessary.
    try:
        with open(filename, 'rb', bufsize) as stream:
            # After some testing - this simple idiom is often the fastest.
            for line in stream:
                yield line
    except GeneratorExit:  # respond to g.close()
        pass
