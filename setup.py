"""Package metadata for crawdad.
"""
from distutils.core import setup

with open("README.rst", "r") as readme_file:
    README_TEXT = readme_file.read()

setup(
    name="crawdad",
    version="0.0.0",
    author="Sasha Hart",
    url="http://github.com/sashahart/crawdad",
    author_email="s@sashahart.net",
    license="MIT",
    packages=["crawdad"],
    scripts=["scripts/crawdad"],
    long_description=README_TEXT,
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3.2",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
    ],
)
